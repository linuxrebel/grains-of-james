import subprocess
def kern():
  inKern=subprocess.check_output("rpm -qa | grep kernel | grep -v dracut | grep -v tools | grep -v header | grep -v firmware | cut -d - -f 2-3 | cut -d . -f 1-5 ", shell=True)
  inKern = inKern.split('\n')[0]
  rKern=subprocess.check_output('uname -r | cut -d . -f 1-5', shell=True)
  rKern = rKern.rstrip("\r\n")
  grains = {}
  if inKern == rKern:
    grains['reboot_required'] = 'no'
  else:
    grains['reboot_required'] = 'yes'
  return grains
