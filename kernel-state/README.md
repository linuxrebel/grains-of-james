# Detection of Running Kernel vs Newest Installed

## Concept
You need to know if the system in question has been restarted into the newest installed kernel. To do this you have to detect what is running and what the latest version installed is.  This data can also be used to detect if the version installed is the version you want installed (The "I put the newest kernel in the rpm repo" syndrome)

## Method
doing an rpm query, and some text manipulation, it will find out the "name" of the latest version installed.  Using uname it will get the running version.  It will report both. Then in addition it compares the two values, to determine if it's running on the newest installed version, then will report if a reboot is required for this system.

## Security implications
It does you no good to install newer versions of the kernel to get around a Vulnerability, if you keep running the old version.  You won't hear the bad guy saying "Ooo they installed a new version, so I can't exploit the vuln in the older running version." ever.  You can use the value of the grain "reboot_required yes" to signal to your monitoring, that the system is in need of a reboot and schedule accordingly. 
