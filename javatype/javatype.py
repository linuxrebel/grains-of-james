###########################################
#
# James Sparenberg
# April 2020
# WFH month 2
# All vars are either True or Falst (0-1)
# wjava = Which Java test if java is in path
# orjdk = Test for the std Oracle rpm
# ajdk = Test for Amazon Corretto JDK
###########################################

import subprocess
import fnmatch
import os

def jtype():
  wjava=subprocess.call('which java > /dev/null 2>&1', shell=True)
  grains = {}
  if wjava == 0:
    orjdk=subprocess.call('rpm -qa | grep jdk | grep linux > /dev/null 2>&1', shell=True)
    ojdk=subprocess.call('rpm -qa | grep openjdk > /dev/null 2>&1', shell=True)
    ajdk=subprocess.call('rpm -qa | grep corretto > /dev/null 2>&1', shell=True)

    if orjdk == 0:
      javaname=subprocess.check_output('rpm -qa | grep jdk | grep linux', shell=True)
      grains['System-Java'] = 'Oracle-Std', javaname
    elif ojdk == 0:
      javaname=subprocess.check_output('rpm -qa | grep openjdk | grep -v javadoc | grep -v devl | grep -v jmods | grep -v debug | grep -v src | grep -v demo | ', shell=True)
      grains['System-Java'] = 'OpenJDK', javaname
    if ajdk == 0:
      javaname=subprocess.check_output('rpm -qa | grep -i corretto', shell=True)
      grains['System-Java'] = 'Amazon-Corretto', javaname
  else:
    grains['System-Java'] = 'None'
  return grains
