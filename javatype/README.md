# Java Version Installed Detection

## Concept
Java, expecially on Red Hat derived Linux doesn't upgrade in place.  Instead in order to not break running java processes, (supposedly) it installes in parallel and then changes symlinks.  This is designed to detect all vrersions of java installed, and define the single version that the command java is symlinked to.

## Method
running rpm queryies we find OpenJDK, OracleJDK or Amazon Corretto JDK.  We strip and report ALL install versions. 

## Security implications
Just because you installed the newest version of Java on your systems, it doesn't mean that the older, vulnerable version, is not still usable.  Or even that it isn't in use.  This allows you to find out fully what is installed and take corrective action.  It also will detect that cowboy dev/admin who, in order to "Just get it working" installed an older JDK and hard coded paths. You can now trigger on the change and alert accordingly.
