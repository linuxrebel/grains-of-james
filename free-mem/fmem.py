import subprocess

def memf():
  x=subprocess.check_output(["free"])
  y=x.split()
  grains = {}
  grains['totalmem'] = y[7]
  grains['usedmem'] = y[8]
  grains['freemem'] = y[9]
  return grains
