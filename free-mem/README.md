# Memory Usage detection

## Concept
Use the free command to detect the free memory on a system. This outputs Free, Used, and Total mem.  Could be charted into your monitoring to track for memory leaks

## Method
uses subprocess to get info

## Security implications
Not heavy on security.  Largely just a "first grain" for me to learn Python and grains
