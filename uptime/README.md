# System uptime detection

## Concept
Using the uptime command in Linux, figuring out how long since last reboot

## Method
The results of the uptime command are parsed and put into a grain.

## Security implications
If you are in a kill/fill environment and objects/systems are replaced and updated every X days.  Then knowing you have a system that wasn't restarted is usefull.  
