###########################################
#
# James Sparenberg
# Feb 2020
# for Lending Club
#
# This determines if oracle db rpm is installed or not
###########################################

import subprocess
def elem():
  sname=subprocess.call('find /opt | grep sqlplus > /dev/null 2>&1', shell=True)
  grains = {}
  if sname == 0:
    grains['oracleDB'] = 'yes'
  elif sname == 1:
    grains['oracleDB'] = 'no'
  else:
    grains['oracleDB'] = 'no'
  return grains
