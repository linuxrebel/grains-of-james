# Oracle DB Detection

## Concept
Oracle is not always so nicely installed (ala rpm or deb) This is for those corner cases where it's installed with the normal oracle installer/tools rather than by other means. Oracle puts it in /opt (Legit) and the key is a file called sqlplus.  Without that file oracle doesn't run as this is the main command to start/stop the DB

## Method
simple find in /opt detects the existance of the sqlplus binary.  Now you can detect that yes, this is a DB and no I shouldn't blindly start updating things unless I want to take down the company.

## Security implications
Security is not just prevention of intrusion.  It also, as in this case, can be prevention of data loss.  By knowing for sure that the system is an oracle DB you are able to build automation that will occomodate this need.  
