# Corrupted RPM Database detection

## Concept
The idea here is to detect that a rpm based system has a corrupted rpm database so that it can be corrected via a state file. Since it is impractacle to bother fixing every DB on every system when their are thousands of them.  This allows you to target just the corrupted ones.

## Method
When a Systems DB is corrupted something as simple as a search for an uninstalled rpm causes error.  Emacs was chosen as this is not a part of the dev stack used on the systems in question.  You can change this as needed.  If it gets a "0" for a return.  The DB is marked as "no" meaning it doesn't need to be cleaned.

## Security implications
A corrupted DB will give you a false "No updates needed" state when running a large scale upgrade cycle.  This is turn gives you a false sense of security about you are managing to keep up to date on CVE mitigation.