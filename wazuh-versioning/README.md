# Wazuh install detection

## Concept
rpm query tells you if the rpm is installed, then additonal queries are run for "is it running" and what version

## Method
Normal Linux CLI queries.

## Security implications 
Wazuh is the keeper of OSSEC the industry standard for File Integrety Monitoring.  As such it is critical to know if the number of installed systems is equal to the number of systems under FIM control.  Additionally knowing the version installed assists in keeping you under control.
