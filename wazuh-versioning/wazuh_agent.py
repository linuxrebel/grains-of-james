import subprocess
def elem():
  sname=subprocess.call('rpm -qa | grep wazuh > /dev/null 2>&1', shell=True)
  grains = {}
  if sname == 0:
    istate = 'yes'
    rname=subprocess.check_output('rpm -qa | grep wazuh', shell=True)
    grains['wazuh_installed'] = istate, rname
  else:
    istate = 'no' 
    grains['wazuh_installed'] = istate, 'none'
  return grains
