# Linux Snow Agent Installation Detection

## Concept
We had an inventory (physical and application) tracking product called snow-agent.  What was needed was a way to track, if, it was installed.  Then what version.  This provides those two data points.

## Method
It does an rpm query for the snow rpm and then once found records both the existance of the install (or not) and the version if installed.  If not installed no grain is returned

## Security implications
Since this was a critical item for control of applications (License costs) etc, know this info is required for certain compliance issues.  
